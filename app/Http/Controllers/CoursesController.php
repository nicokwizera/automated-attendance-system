<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Enrollment;
use App\Models\Student;
use App\Models\Attendance;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function store(Request $request)
    {
        Course::create([
            'course_code' => $request->course_code,
            'course_name' => $request->course_name,
            'semester'=> $request->semester,
            'year'=> $request->year,
            'lecturer_id' => \Auth::guard('lecturer')->user()->id,
        ]);
        return redirect('/lecturer');
    }

    public function attendance($id)
    {
        $course = Course::find($id);
        $students = Enrollment::first()->students;
        $enrollment = Course::find($id)->enrollment;
        return view('lecturer.take-attendance', ['course' => $course, 'students' => $students, 'enrollments' => $enrollment]);
    }

    public function take_attendance(Request $request)
    {
//        dd($request->all());
        $input = $request->all();
        for ($i = 0; $i<=count($input['student']); $i++){
            if (empty($input['student'][$i]) || !is_numeric($input['student'][$i])) continue;

            $data = [
                'attendance_status' => $input['status'][$i],
                'lecturer_id' => $input['lecturer'][$i],
                'enrollment_id' => $input['course'][$i],
                'student_id' => $input['student'][$i],
            ];
            Attendance::create($data);
        }
        return "success";

    }

    public function courses()
    {
        $courses = Course::where('lecturer_id', \Auth::guard('lecturer')->user()->id)->get();
        return view('lecturer.show-courses', ['courses' => $courses]);
    }

    public function destroy($id)
    {
        $course = Course::find($id)->delete();
        return redirect('/lecturer/courses');
    }
}
