<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Models\Enrollment;

class EnrollmentsController extends Controller
{
    public function create($id)
    {
        $enrollment = new Enrollment;
        $enrollment->save();
        $course = Course::find($id);
        $student = Student::find(\Auth::guard('student')->user()->id);
        $enrollment->courses()->attach($course);
        $enrollment->students()->attach($student);
        return 'success';
    }

    public function showEnrolledCourses()
    {
        $enrollments = Enrollment::first();
        $courses = $enrollments->courses;
        return view('lecturer.show-enrolled-courses', ['courses' => $courses]);
    }

}
