<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Get the lecturer that created the course
     */
    public function lecturer()
    {
        return $this->belongsTo('App\Models\Lecturer');
    }

    public function enrollment()
    {
        return $this->belongsToMany('App\Models\Enrollment');
    }
}
