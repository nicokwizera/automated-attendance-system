<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Get the attendance for an enrolled course
     */
    public function attendances()
    {
        return $this->hasMany('App\Models\Attendance');
    }

    /**
     * The Students that belong to the enrollment
     */
    public function students()
    {
        return $this->belongsToMany('App\Models\Student');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Models\Course');
    }
}
