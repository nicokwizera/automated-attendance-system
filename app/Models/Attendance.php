<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * get the lecturers that took the attendance
     */
    public function lecturer()
    {
        return $this->belongsTo('App\Models\Lecturer');
    }

    /**
     * get the students that attended class
     */
    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    /**
     * get the Course that was attendance
     */
    public function enrollment()
    {
        return $this->belongsTo('App\Models\Enrollment');
    }
}
