<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lecturer extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $guard = 'lecturer';

    protected $fillable = [
        'first_name', 'last_name', 'phone_number' , 'email', 'password'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Get the courses created by a lecturer
     */
    public function courses()
    {
        return $this->hasMany('App\Models\Course');
    }

    /**
     * Get the attendances taken by a lecturer
     */
    public function attendances()
    {
        return $this->hasMany('App\Models\Attendance');
    }
}
