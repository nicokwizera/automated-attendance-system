<?php

namespace Database\Factories;

use App\Models\Course;
use App\Models\Lecturer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Course::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'course_code' => 'BSE3089',
            'course_name' => 'Software Engineering',
            'semester' => 'II',
            'year' => $this->faker->year,
            'lecturer_id' => Lecturer::factory(),
        ];
    }
}
