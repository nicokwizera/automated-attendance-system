<?php

namespace Database\Factories;

use App\Models\Attendance;
use App\Models\Enrollment;
use App\Models\Lecturer;
use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class AttendanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Attendance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'attendance_status' => 'Present',
            'lecturer_id' => Lecturer::factory(),
            'enrollment_id' => Enrollment::factory(),
            'student_id' => Student::factory(),
        ];
    }
}
