<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CoursesController;
use App\Http\Controllers\EnrollmentsController;
use App\Models\Course;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/student/courses', function (){
    return view('student.courses', [
        'courses' => Course::all()
    ]);
});



Auth::routes();

Route::get('/login/lecturer', [LoginController::class, 'showLecturerLoginForm']);
Route::get('/login/student', [LoginController::class,'showStudentLoginForm']);
Route::get('/register/lecturer', [RegisterController::class,'showLecturerRegisterForm']);
Route::get('/register/student', [RegisterController::class,'showStudentRegisterForm']);
Route::get('/student/enrollment/create/{course}', [EnrollmentsController::class, 'create']);
Route::get('/lecturer/courses/enrolled', [EnrollmentsController::class, 'showEnrolledCourses'])->middleware('auth:lecturer');
Route::get('/lecturer/courses/attendance/{course}', [CoursesController::class, 'attendance'])->middleware('auth:lecturer');
Route::get('lecturer/courses', [CoursesController::class, 'courses'])->middleware('auth:lecturer');
Route::get('/lecturer/courses/{course}', [CoursesController::class, 'destroy']);

Route::post('/login/lecturer', [LoginController::class,'lecturerLogin']);
Route::post('/login/student', [LoginController::class,'studentLogin']);
Route::post('/register/lecturer', [RegisterController::class,'createLecturer']);
Route::post('/register/student', [RegisterController::class,'createStudent']);
Route::post('/courses', [CoursesController::class, 'store']);
Route::post('/attendance', [CoursesController::class, 'take_attendance']);

Route::group(['middleware' => 'auth:lecturer'], function () {
    Route::view('/lecturer', 'lecturer.dashboard');
});

Route::group(['middleware' => 'auth:student'], function () {
    Route::view('/student', 'student.dashboard');
});

Route::get('logout', [LoginController::class,'logout']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
