@extends('base')
@section('content')
    <p>Choose from the courses to enroll</p>
    <div class="row">
        @foreach($courses as $course)
            <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-black o-hidden h-100">
                <div class="card-header text-center"><h5>COURSE</h5></div>
                <div class="card-body">
                    <div class="mr-5">Course Code: <span>{{ $course->course_code }}</span></div>
                    <div class="mr-5">Course Name: <span>{{ $course->course_name }}</span></div>
                    <div class="mr-5">Semester: <span>{{ $course->semester }}</span></div>
                </div>
                <div class="card-footer text-center">
                    <a href="/student/enrollment/create/{{ $course->id }}" class="btn btn-primary">Enroll In Course</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
