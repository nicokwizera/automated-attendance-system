@extends('base')
@section('content')
    <p>Mark Attendance for the Students</p>
    <div class="row">
        @foreach($courses as $course)
            @if( $course->lecturer_id === \Auth::guard('lecturer')->user()->id)
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-black o-hidden h-100">
                        <div class="card-header text-center"><h5>COURSE</h5></div>
                        <div class="card-body">
                            <div class="mr-5">Course Code: <span>{{ $course->course_code }}</span></div>
                            <div class="mr-5">Course Name: <span>{{ $course->course_name }}</span></div>
                            <div class="mr-5">Semester: <span>{{ $course->semester }}</span></div>
                            <div class="mr-5">Lecturer id: <span>{{ $course->lecturer_id }}</span></div>
                        </div>
                        <div class="card-footer text-center">
                            <a href="/lecturer/courses/attendance/{{$course->id}}" class="btn btn-primary">Take Attendance</a>
                        </div>
                    </div>
                </div>
            @else
                <p>No student has enrolled in your courses</p>
            @endif
        @endforeach
    </div>
@endsection
