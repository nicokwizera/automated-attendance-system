@extends('base')
@section('content')
    <p> Welcome Kwizera Nicholas</p>
    <p>Click to Add the class</p>
    <a href="/lecturer/courses" class="btn btn-primary" role="button">View Classes</a>
    <a href="/lecturer/courses/enrolled" class="btn btn-primary" role="button">Mark Attendance</a>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Class</button>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal Title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                <div class="modal-body">
                                <form action="/courses" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="course_code">Course Code</label>
                                        <input type="text" class="form-control" id="course_code" name="course_code" aria-describedby="emailHelp" placeholder="eg BSE2019">
                                    </div>
                                    <div class="form-group">
                                        <label for="course_name">Course Name</label>
                                        <input type="text" class="form-control" id="course_name" name="course_name" placeholder="Eg Software Engineering">
                                    </div>
                                    <div class="form-group">
                                        <label for="semester">Semester</label>
                                        <select name="semester" id="semester" class="form-control">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="year">Year</label>
                                        <input type="text" class="form-control" id="year" name="year" placeholder="Eg 2021">
                                    </div>
                                    <button type="submit" class="btn btn-primary">CREATE CLASS</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
                                </form>
                            </div>
            </div>
        </div>
    </div>
@endsection
