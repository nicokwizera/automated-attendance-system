@extends('base')
@section('content')
    <p>Take Attendance for {{$course->course_name}}</p>
    <form method="post" action="/attendance">
        @csrf
        <table class="table">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th><abbr title="Registration Number">Reg No</abbr></th>
                <th>Attendance Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($students as $student)
                <tr>
                    <td>{{$student->first_name}}</td>
                    <td>{{$student->last_name}}</td>
                    <td>{{$student->registration_number}}</td>
                    <td>
                        <select name="status[]">
                            <option value="present">Present</option>
                            <option value="absent">Absent</option>
                        </select>
                    </td>
                </tr>
                <input type="hidden" name="student[]" value="{{$student->id}}">
                <input type="hidden" name="lecturer[]" value="{{ \Auth::guard('lecturer')->user()->id }}">
                @foreach($enrollments as $enrollment)
                <input type="hidden" name="course[]" value="{{$enrollment->id}}">
                @endforeach
            @endforeach
            </tbody>
        </table>
        <button type="submit">Submit</button>
@endsection
